json.extract! concept, :id, :name, :stage, :dvf, :current_valuation, :future_valuation, :created_at, :updated_at#, :notes
json.url concept_url(concept, format: :json)
