module ConceptsHelper
  def setup_concept(concept)
    notes = concept.notes
    notes.present? ? notes : 3.times {notes.build}
    concept
  end
end
