class CreateConcepts < ActiveRecord::Migration[5.1]
  def change
    create_table :concepts do |t|
      t.string :name
      t.string :stage
      t.float :dvf
      t.integer :current_valuation
      t.integer :future_valuation

      t.timestamps
    end
  end
end
